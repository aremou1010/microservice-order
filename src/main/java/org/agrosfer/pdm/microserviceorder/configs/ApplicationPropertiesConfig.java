/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceorder.configs;

import java.util.List;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 *
 * @author g3a
 */
@Component
@ConfigurationProperties(prefix = "ms-order-configs")
@Data
public class ApplicationPropertiesConfig {
    
    String url;
    String msCodificationCreateUrl;
    String msHistoryCreateUrl;
    String measureUnit;
    
    List<String> types;
    List<String> categories;
    
}
