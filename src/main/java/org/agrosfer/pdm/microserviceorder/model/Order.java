/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceorder.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author g3a
 */
@Data
@Entity
@Table(name = "orders")
public class Order implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    private String reference;
    
    private String designation;
    
    private String type;
    
    private String category;
    
    private OrderStatus status;
    
    private Timestamp availableFrom;
    
    private Timestamp availableTo;
    
    private Double bid;
    
    private Double ask;
    
    private Double lastPrice;
    
    private Double closePrice;
    
    private Double quantity;
    
    private Boolean isSplit;
    
    private Double minQuantity;
    
    private String measureUnit;
    
    private Boolean isActivated;
    
    private String numberOfView;
    
    private Timestamp createAt;
    
    private Timestamp updateAt;
    
    private Timestamp deleteAt;
    
    private Boolean isDelete;
    
    private String userRef;

    public Order() {
    }

    public Order(Long id, String reference, String designation, String type, String category, OrderStatus status, Timestamp availableFrom, Timestamp availableTo, Double bid, Double ask, Double lastPrice, Double closePrice, Double quantity, Boolean isSplit, Double minQuantity, String measureUnit, Boolean isActivated, String numberOfView, Timestamp createAt, Timestamp updateAt, Timestamp deleteAt, Boolean isDelete, String userRef) {
        this.id = id;
        this.reference = reference;
        this.designation = designation;
        this.type = type;
        this.category = category;
        this.status = status;
        this.availableFrom = availableFrom;
        this.availableTo = availableTo;
        this.bid = bid;
        this.ask = ask;
        this.lastPrice = lastPrice;
        this.closePrice = closePrice;
        this.quantity = quantity;
        this.isSplit = isSplit;
        this.minQuantity = minQuantity;
        this.measureUnit = measureUnit;
        this.isActivated = isActivated;
        this.numberOfView = numberOfView;
        this.createAt = createAt;
        this.updateAt = updateAt;
        this.deleteAt = deleteAt;
        this.isDelete = isDelete;
        this.userRef = userRef;
    }
    
}
