/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceorder.model;

/**
 *
 * @author g3a
 */
public enum OrderStatus {

    IN_WAITING, ACTIVE, SUSPEND, CANCELLED, EXPIRED;

    public boolean valid(OrderStatus currentStatus, OrderStatus newStatus) {

        if (currentStatus == IN_WAITING) {
            return newStatus == ACTIVE || newStatus == CANCELLED || newStatus == EXPIRED;
        } else if (currentStatus == ACTIVE) {
            return newStatus == SUSPEND || newStatus == EXPIRED;
        } else if (currentStatus == SUSPEND) {
            return newStatus == ACTIVE || newStatus == EXPIRED;
        } else if (currentStatus == CANCELLED || currentStatus == EXPIRED) {
            return false;
        } else {
            throw new RuntimeException("Unrecognized situation.");
        }
    }

}
