/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceorder.web.controller;

import java.net.URI;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Optional;
import org.agrosfer.pdm.microserviceorder.beans.HistoryBean;
import org.agrosfer.pdm.microserviceorder.beans.ReferenceBean;
import org.agrosfer.pdm.microserviceorder.configs.ApplicationPropertiesConfig;
import org.agrosfer.pdm.microserviceorder.dao.OrderDAO;
import org.agrosfer.pdm.microserviceorder.model.Order;
import org.agrosfer.pdm.microserviceorder.model.OrderStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author g3a
 */
@RestController
public class OrderController {
    
    @Autowired
    OrderDAO orderDAO;
    
    @Autowired
    RestTemplate restTemplate;
    
    @Autowired
    ApplicationPropertiesConfig apc;
    
    @PostMapping("/orders")
    public ResponseEntity<?> createOrder(@RequestBody Order order){
        
        order.setMeasureUnit(apc.getMeasureUnit());
        order.setCreateAt(Timestamp.from(Instant.now()));
        order.setUpdateAt(Timestamp.from(Instant.now()));
        order.setIsDelete(Boolean.FALSE);
        order.setStatus(OrderStatus.IN_WAITING);
        order.setIsActivated(Boolean.FALSE);
        
        HistoryBean history = new HistoryBean();
        history.setObject("ORDER");
        history.setAction("CREATE");
        history.setUserReference(order.getUserRef());
        
        if (order.getUserRef() == null || order.getUserRef().isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("USER NOT FOUND");
        }
        
        String ref = "";
        ref = getReference(Order.class.getSimpleName());

        if (ref == "") {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Order cannot be created. Go to check microservice-codification");
        }
        
        order.setReference(ref);
        Order newOrder = orderDAO.save(order);
        
        if (newOrder != null) {
            history.setObjectReference(newOrder.getReference());
            createHistory(history);
            
            return ResponseEntity.accepted()
                    .location(URI.create(apc.getUrl() + "/orders/" + newOrder.getReference()))
                    .body(newOrder);
        }
        
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Order cannot be created");
    }
    
    @GetMapping("/orders")
    public ResponseEntity<?> findOrders(){
        
        return ResponseEntity.status(HttpStatus.OK).body(orderDAO.findAllByIsDeleteFalse());
        
    }
    
    @GetMapping("/orders/type/{type}")
    public ResponseEntity<?> findOrdersByType(@PathVariable("type") String type){
        
        return ResponseEntity.status(HttpStatus.OK).body(orderDAO.findAllByType(type));
        
    }
    
    @GetMapping("/orders/user/{userRef}")
    public ResponseEntity<?> findOrdersByUser(@PathVariable("userRef") String userRef){
        
        return ResponseEntity.status(HttpStatus.OK).body(orderDAO.findAllByUserRefAndIsDeleteFalse(userRef));
        
    }
    
//    @PutMapping("/orders")
    @PostMapping("/orders/update")
    public ResponseEntity<?> updateOrders(@RequestBody Order order){
        
        order.setUpdateAt(Timestamp.from(Instant.now()));
        
        Order updateOrder = orderDAO.saveAndFlush(order);
        
        if (updateOrder != null) {
            return ResponseEntity.status(HttpStatus.OK).body(updateOrder);
        }
        
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Order cannot be updated.\n Retry later");
    }
    
    @GetMapping("/orders/id/{id}")
    public ResponseEntity<?> findOrderById(@PathVariable("id") Long id){
        
        Optional<Order> aOrder;
        aOrder = orderDAO.findByIdAndIsDeleteFalse(id);
        
        if (aOrder.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(aOrder);
        }
        
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("This order does not exist");
    }
            
    @GetMapping("/orders/{ref}")
    public ResponseEntity<?> findOrderByRef(@PathVariable("ref") String ref){
        
        Optional<Order> aOrder;
        aOrder = orderDAO.findByReferenceAndIsDeleteFalse(ref);
        
        if (aOrder.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(aOrder);
        }
        
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("This order does not exist");
    }
    
//    @PutMapping("/orders/{ref}/validate")
    @PostMapping("/orders/{ref}/validate")
    public ResponseEntity<?> validateOrder(@PathVariable("ref") String ref){
        
        Optional<Order> aOrder;
        aOrder = orderDAO.findByReferenceAndIsDeleteFalse(ref);
        
        if (aOrder.isPresent()) {
            OrderStatus orderStatus = OrderStatus.IN_WAITING;
            if (orderStatus.valid(aOrder.get().getStatus(), OrderStatus.ACTIVE)) {
                aOrder.get().setStatus(OrderStatus.ACTIVE);
                aOrder.get().setIsActivated(Boolean.TRUE);

                Order activeOrder = null;
                activeOrder = orderDAO.saveAndFlush(aOrder.get());
                if (activeOrder == null) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Order cannot been activated. Retry later");
                }
                return ResponseEntity.status(HttpStatus.OK).body("Order is activated successfully");
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Transitioning from " + aOrder.get().getStatus() + " to " + OrderStatus.ACTIVE + " is not valid.");

        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("This order does not exist.");
    }
    
//    @PutMapping("/orders/{ref}/suspend")
    @PostMapping("/orders/{ref}/suspend")
    public ResponseEntity<?> suspendOrder(@PathVariable("ref") String ref){
        
        Optional<Order> aOrder;
        aOrder = orderDAO.findByReferenceAndIsDeleteFalse(ref);
        
        if (aOrder.isPresent()) {
            OrderStatus orderStatus = OrderStatus.IN_WAITING;
            if (orderStatus.valid(aOrder.get().getStatus(), OrderStatus.SUSPEND)) {
                aOrder.get().setStatus(OrderStatus.SUSPEND);

                Order suspendOrder = null;
                suspendOrder = orderDAO.saveAndFlush(aOrder.get());
                if (suspendOrder == null) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Order cannot been suspend. Retry later");
                }
                return ResponseEntity.status(HttpStatus.OK).body("Order is suspend successfully");
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Transitioning from " + aOrder.get().getStatus() + " to " + OrderStatus.SUSPEND + " is not valid.");

        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("This order does not exist.");
        
    }
    
//    @PutMapping("/orders/{ref}/cancelled")
    @PostMapping("/orders/{ref}/cancelled")
    public ResponseEntity<?> cancelOrder(@PathVariable("ref") String ref){
        
        Optional<Order> aOrder;
        aOrder = orderDAO.findByReferenceAndIsDeleteFalse(ref);
        
        if (aOrder.isPresent()) {
            OrderStatus orderStatus = OrderStatus.IN_WAITING;
            if (orderStatus.valid(aOrder.get().getStatus(), OrderStatus.CANCELLED)) {
                aOrder.get().setStatus(OrderStatus.CANCELLED);
                //aOrder.get().setIsActivated(Boolean.TRUE);

                Order cancelOrder = null;
                cancelOrder = orderDAO.saveAndFlush(aOrder.get());
                if (cancelOrder == null) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Order cannot been cancelled. Retry later");
                }
                return ResponseEntity.status(HttpStatus.OK).body("Order is cancelled successfully");
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Transitioning from " + aOrder.get().getStatus() + " to " + OrderStatus.CANCELLED + " is not valid.");

        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("This order does not exist.");
        
    }
    
    @DeleteMapping("/orders/{ref}")
    public ResponseEntity<?> deleteOrder(@PathVariable("ref") String ref){
        
        Optional<Order> aOrder;
        aOrder = orderDAO.findByReferenceAndIsDeleteFalse(ref);
        
        if (aOrder.isPresent()) {
            aOrder.get().setIsDelete(Boolean.TRUE);
            aOrder.get().setDeleteAt(Timestamp.from(Instant.now()));
            
            Order deletedOrder = null;
            deletedOrder = orderDAO.saveAndFlush(aOrder.get());
            if (deletedOrder == null) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Order cannot been deleted. Retry later");
            }
            return ResponseEntity.status(HttpStatus.OK).body("Order is delete successfully");
        }
        
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("This order does not exist.");
    }
    
    @GetMapping("/orders/types")
    public ResponseEntity<?> getTypes(){
        
        return ResponseEntity.ok(apc.getTypes());
        
    }
    
    @GetMapping("/orders/categories")
    public ResponseEntity<?> getCategories(){
        
        return ResponseEntity.ok(apc.getCategories());
        
    }
    
    public String getReference(String entityName) {
        ReferenceBean rb = new ReferenceBean();
        rb.setEntity(entityName);

        //Method1
        HttpEntity<?> request = new HttpEntity<>(rb);
        ResponseEntity<ReferenceBean> re;
        try {
            re = restTemplate.exchange(URI.create(apc.getMsCodificationCreateUrl()), HttpMethod.POST, request, ReferenceBean.class);
            return re.getBody().getCode();

        } catch (RestClientException e) {
            return ("");
        }

        //Method2
        //ReferenceBean newR = restTemplate.postForObject(URI.create(apc.getMsCodificationCreateUrl()), rb, ReferenceBean.class);
        //return ResponseEntity.ok(newR);
    }
    
    public ResponseEntity<?> createHistory(HistoryBean historyBean) {
        HistoryBean hb = new HistoryBean();
//        rb.setEntity(entityName);

        //Method1
        HttpEntity<?> request = new HttpEntity<>(historyBean);
        ResponseEntity<?> re;
        try {
            re = restTemplate.exchange(URI.create(apc.getMsHistoryCreateUrl()), HttpMethod.POST, request, HistoryBean.class);
            return re;

        } catch (RestClientException e) {
            return ResponseEntity.of(Optional.of(e));
        }
        
    }
    
}
