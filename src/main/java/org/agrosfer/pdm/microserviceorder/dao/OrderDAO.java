/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceorder.dao;

import java.util.List;
import java.util.Optional;
import org.agrosfer.pdm.microserviceorder.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author g3a
 */
public interface OrderDAO extends JpaRepository<Order, Long> {
    
    Optional<Order> findByReferenceAndIsDeleteFalse(String ref);
    
    List<Order> findAllByIsDeleteFalse();
    
    List<Order> findAllByIsDeleteTrue();
    
    List<Order> findAllByUserRefAndIsDeleteFalse(String userRef);
    
    Optional<Order> findByIdAndIsDeleteFalse(Long id);
    
    List<Order> findAllByType(String type);
    
}
